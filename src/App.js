import React, {useState} from 'react';
import Tweet from './Tweet';
import './App.css';

function App() {
const [users,setUser] = useState([
  {name:"Chiag Mai" , message: "Good place for bike"},
  {name:"Jonh Wick" , message: "Nice movie!"},
  {name:".Net Core" , message: "It's awesome."},
  {name:"New record", message:"Try to break history"}
  ]);

  return (
    <div className="app">    
      {users.map(user =>  (
        <Tweet name={user.name} message={user.message} />
      ))}     
    </div>    
  );
}

export default App;
